package com.example.restauration_gastronomica_mobile;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AndroidDB extends SQLiteOpenHelper{

    public AndroidDB(Context ctx){
        super(ctx,"BaseAndroid",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE reservation (id_reservation INTEGER PRIMARY KEY);");

        //http://sampoujol.free.fr/Android-ExempleBD.zip//pour faire apparaître des informations dans le logcat, très pratique pour debugger
        Log.i("", "Création de la base de données réussie");
    }

    @Override
    public void onUpgrade(SQLiteDatabase de, int arg1, int arg2) {
        // TODO Auto-generated method stub

    }
}

