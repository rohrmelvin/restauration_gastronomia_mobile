package com.example.restauration_gastronomica_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class connexion extends AppCompatActivity {
    RequestQueue fileWS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seconnecter);
    }

    public void connexion(View v){
        fileWS= Volley.newRequestQueue(this);

        TextView crPrenom=findViewById(R.id.Insertion_Prenom);
        TextView crNom=findViewById(R.id.Insertion_Nom);
        EditText crMail=findViewById(R.id.Insertion_Email);


        requestConnexion(crPrenom.getText().toString(),crNom.getText().toString(),crMail.getText().toString());


    }

    public void requestConnexion(String prenom_user, String nom_user, String email_user){
        String url="http://10.0.2.2/~melvin.rohr/restauration_gastronomica/public/ws/InfosUser"+ prenom_user +"/" + nom_user +"/" + email_user;
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                this::processusConnexion,
                this::gereErreursWs);

        fileWS.add(stringRequest);
    }


    public void processusConnexion(String reponse){
        try {
            JSONArray ja = new JSONArray(reponse);
            JSONObject j = ja.getJSONObject(0);
            String PrenomUtilisateur=j.getString("prenom_utilisateur");
            String NomUtilisateur=j.getString("nom_utilisateur");
            String MailUtilisateur=j.getString("mail_utilisateur");

            Bundle extras = getIntent().getExtras();
            String value = String.valueOf(extras.getString(""));
            Log.i(""," Value : "+ value);

                Intent i= new Intent(this,connexion.class);
                i.putExtra("Prenom",PrenomUtilisateur);
                i.putExtra("Nom",NomUtilisateur);
                i.putExtra("Mail",MailUtilisateur);
                i.putExtra("isLogin",true);
                startActivity(i);

            Toast.makeText(this, "Connexion réussite", Toast.LENGTH_LONG).show();
        }
        catch(JSONException jx){
            gereErreursWs(jx);
        }
    }

    public void gereErreursWs(Throwable t){
        Toast.makeText(this,
                "Problème de communication avec le serveur",
                Toast.LENGTH_LONG).show();

        Log.e("Userconnexion",
                Log.getStackTraceString(t));
    }
}
