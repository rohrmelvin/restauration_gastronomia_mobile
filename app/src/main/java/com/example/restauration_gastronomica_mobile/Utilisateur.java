package com.example.restauration_gastronomica_mobile;

public class Utilisateur {
    public Utilisateur(String Prenom_utilisateur, String Nom_utilisateur, String Mail_utilisateur){
        this.Prenom_utilisateur = Prenom_utilisateur;
        this.Nom_utilisateur = Nom_utilisateur;
        this.Mail_utilisateur = Mail_utilisateur;
    }

    private String Prenom_utilisateur;
    private String Nom_utilisateur;
    private String Mail_utilisateur;

    public String getPrenom_utilisateur() {
        return Prenom_utilisateur;
    }
    public void setPrenom_utilisateur(String prenom_utilisateur) {Prenom_utilisateur = prenom_utilisateur;}

    public String getNom_utilisateur() {
        return Nom_utilisateur;
    }
    public void setNom_utilisateur(String nom_utilisateur) {Nom_utilisateur = nom_utilisateur;}

    public String getMail_utilisateur() {
        return Mail_utilisateur;
    }
    public void setMail_utilisateur(String mail_utilisateur) {Mail_utilisateur = mail_utilisateur;}
}
